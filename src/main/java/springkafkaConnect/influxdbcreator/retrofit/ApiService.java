package springkafkaConnect.influxdbcreator.retrofit;

import retrofit2.Call;
import retrofit2.http.*;
import springkafkaConnect.influxdbcreator.model.Connect;

public interface ApiService {

    @Headers({"Accept: application/json"})
    @POST("/connectors")
    Call<Connect> createConnect(@Body Connect connect);

    @DELETE("/connectors/{name}")
    Call<Void>  deleteConnect(@Path("name") String name);

}
