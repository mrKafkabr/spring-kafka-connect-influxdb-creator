package springkafkaConnect.influxdbcreator.retrofit;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import springkafkaConnect.influxdbcreator.model.Connect;

import java.io.IOException;

@Service
public class APIClient {

    private final String URI;

    public APIClient(@Value("${kafka.connect.uri}") String uri) {
        URI = uri;
    }

    public Connect createInfluxConnect(Connect connect) throws IOException {

        Retrofit retrofit = getRetrofit();

        ApiService apiService = retrofit.create(ApiService.class);
        Call<Connect> connect1 = apiService.createConnect(connect);
        return connect1.execute().body();
    }

    public void deleteInfluxConnect(String name) throws Exception {
        Retrofit retrofit = getRetrofit();
        ApiService apiService = retrofit.create(ApiService.class);
        try{
            Call<Void> responseBodyCall = apiService.deleteConnect(name);
            responseBodyCall.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    System.out.println(">>>> " + response);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable throwable) {
                    System.out.println("<<<<< Chamada falhou");
                }
            });

        }catch (Exception e){
            System.out.println(e.getMessage());
            throw new Exception(e);
        }

    }

    private Retrofit getRetrofit() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        return new Retrofit.Builder()
                .baseUrl(URI)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }
}
