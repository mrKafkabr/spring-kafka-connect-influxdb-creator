package springkafkaConnect.influxdbcreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfluxdbCreatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfluxdbCreatorApplication.class, args);
	}

}
