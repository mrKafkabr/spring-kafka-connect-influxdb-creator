package springkafkaConnect.influxdbcreator.kafkaConnectConfig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InfluxConnectConfig {

    @JsonProperty(value = "connector.class")
    private String connectorClass = "com.datamountaineer.streamreactor.connect.influx.InfluxSinkConnector";

    @JsonProperty(value = "connect.influx.db")
    private String connectInfluxDb;

    @JsonProperty(value = "tasks.max")
    private String taskMax = "3";

    @JsonProperty("topics")
    private String topics;

    @JsonProperty("connect.influx.kcql")
    private String connectInfluxKcql;

    @JsonProperty("connect.influx.username")
    private String connectInfluxUsername;

    @JsonProperty("connect.influx.password")
    private String connectInfluxPassword;

    @JsonProperty("connect.influx.url")
    private String connectInfluxUrl = "http://kafka-influxdb:8086";

    @JsonProperty("key.converter.schemas.enable")
    private String keyConverterSchemasEnable = "false";

    @JsonProperty("value.converter.schemas.enable")
    private String valueConverterSchemasEnable = "false";

    @JsonProperty("value.converter")
    private String valueConverter = "org.apache.kafka.connect.json.JsonConverter";

    @JsonProperty("key.converter")
    private String keyConverter = "org.apache.kafka.connect.json.JsonConverter";

    //SETTERS
    public void setConnectInfluxDb(String connectInfluxDb) {
        this.connectInfluxDb = connectInfluxDb;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public void setConnectInfluxKcql(String connectInfluxKcql) {
        this.connectInfluxKcql = connectInfluxKcql;
    }

    public void setConnectInfluxUsername(String connectInfluxUsername) {
        this.connectInfluxUsername = connectInfluxUsername;
    }

    public void setConnectInfluxPassword(String connectInfluxPassword) {
        this.connectInfluxPassword = connectInfluxPassword;
    }
}
