package springkafkaConnect.influxdbcreator.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import springkafkaConnect.influxdbcreator.kafkaConnectConfig.InfluxConnectConfig;
import springkafkaConnect.influxdbcreator.model.Connect;
import springkafkaConnect.influxdbcreator.retrofit.APIClient;

import java.io.IOException;

@Component
public class Consumer {

    private final String TOPIC_CREATE_CONNECT = "createConnect";
    private final String TOPIC_DELETE_CONNECT = "deleteConnect";

    private final String uri;

    @Autowired
    public Consumer(@Value("${kafka.connect.uri}") String uri) {
        this.uri = uri;
    }

    @KafkaListener(topics = {TOPIC_CREATE_CONNECT})
    public void listenerInsert(@Payload String message) {

        Connect connect = createConnect(message);
        try {
            callCreateConnectApi(connect);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @KafkaListener(topics = {TOPIC_DELETE_CONNECT})
    public void listenerDelete(@Payload String message) {
        try {
            callDeleteConnectApi(setConnectName(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callDeleteConnectApi(String connectName) throws Exception {
        APIClient apiClient = new APIClient(uri);
        apiClient.deleteInfluxConnect(connectName);
    }

    private void callCreateConnectApi(Connect connect) throws IOException {
        APIClient apiClient = new APIClient(uri);
        apiClient.createInfluxConnect(connect);
    }

    private Connect createConnect(String name) {
        Connect connect = new Connect();
        connect.setName(setConnectName(name));
        connect.setConfig(configInfluxConnect(name));
        return connect;
    }

    private String setConnectName(String name) {
        return "InfluxSink-" + name;
    }

    private InfluxConnectConfig configInfluxConnect(String name) {
        InfluxConnectConfig influxConnectConfig = new InfluxConnectConfig();
        influxConnectConfig.setConnectInfluxDb("ero");
        influxConnectConfig.setConnectInfluxUsername("root");
        influxConnectConfig.setConnectInfluxPassword("root");
        influxConnectConfig.setTopics(name);
        influxConnectConfig.setConnectInfluxKcql(buildKcql(name));
        return influxConnectConfig;
    }

    private String buildKcql(String name) {
        return "INSERT INTO " + name + " SELECT * FROM " + name + " WITHTIMESTAMP timestamp WITHTAG(equipment)";
    }
}
