package springkafkaConnect.influxdbcreator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import springkafkaConnect.influxdbcreator.kafkaConnectConfig.InfluxConnectConfig;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Connect {

    @JsonProperty("name")
    private String name;

    @JsonProperty("kafkaConfig")
    private InfluxConnectConfig config;

    //GETTERS
    public String getName() {
        return name;
    }

    public InfluxConnectConfig getConfig() {
        return config;
    }

    //SETTERS
    public void setName(String name) {
        this.name = name;
    }

    public void setConfig(InfluxConnectConfig config) {
        this.config = config;
    }
}
